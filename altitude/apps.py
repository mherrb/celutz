# -*- coding: utf-8 -*-

from django.apps import AppConfig


class AltitudeConfig(AppConfig):
    name = 'altitude'
